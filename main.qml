import QtQuick 2.7

Score {
    width: childrenRect.width; height: childrenRect.height
    FontLoader { id: bravura; source: "./Bravura.otf" }
    antialiasing: true
    spacing: 10

    clef: clef
    Clef { id: clef; type: 1 }

    //Note { number: 9; octave: 4; accident: 1 }
    //Note { midiKey: 48 }
    Sequence { model: [36, 40, 43]; spaced: false }
}
